using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class OnKeyClick : NetworkBehaviour
{
    [SerializeField]
    private GameObject head = null;

    private void OnEnable()
    {
        this.transform.position = new Vector3(Random.Range(-3, 3), 0, Random.Range(-3, 3));
    }
    private void Update()
    {
        if (!IsOwner) return;
        if (!Input.GetKeyDown(KeyCode.Space)) return;
        if (head == null) return;

        if (!head.TryGetComponent(out MeshRenderer renderer)) return;

        Color c = Random.ColorHSV();

        ChangeHeadColorServerRpc( c);

        renderer.material.color = c;

        Debug.Log("Owner: " + this.IsOwner + " - LocalPlayer: " + this.IsLocalPlayer + " - Host: " + this.IsHost +
                  " - Client: " + this.IsClient + " - Server: " + this.IsServer);
        Debug.Log("NMLocalClientId: " + NetworkManager.Singleton.LocalClientId + " - OwnerClientId: " + this.OwnerClientId +
                " - Host: " + this.IsHost + " - Client: " + this.IsClient + " - server: " + this.IsServer);
    }
    [ServerRpc(RequireOwnership = false)]
    private void ChangeHeadColorServerRpc(Color newColor)
    {
        Debug.Log("Hey");
        ChangeHeadColorClientRpc(newColor);
    }
    [ClientRpc]
    private void ChangeHeadColorClientRpc(Color newColor)
    {
        Debug.Log("Hola");
        if (IsOwner) return;
        Debug.Log("NotOwner");
        if (!head.TryGetComponent(out MeshRenderer renderer)) return;
        renderer.material.color = newColor;
    }
}

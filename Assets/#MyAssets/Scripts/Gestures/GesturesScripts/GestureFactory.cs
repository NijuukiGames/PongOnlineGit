using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gesture/GestureFactory")]
public class GestureFactory : ScriptableObject
{
    public string Name;
    public List<Vector3> fingersData;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct GestureActionsList
{
    public GestureFactory _gesture;
    public UnityEvent _onRecognized;
}
public class GestureActions : MonoBehaviour
{
    [SerializeField]
    private OVRSkeleton _skeleton;
    private List<OVRBone> _bones;
    [SerializeField]
    private List<GestureActionsList> _actions = new List<GestureActionsList>();

    private GestureActionsList previousAction = new GestureActionsList();

    private float _threshold = 0.1f;

    private void Start()
    {
        _bones = new List<OVRBone>(_skeleton.Bones);
    }

    private void Update()
    {
        GestureActionsList currentAction = Recognize();

        if( !currentAction.Equals(previousAction) )
        {
            previousAction = currentAction;
            currentAction._onRecognized.Invoke();
        }
    }

    private GestureActionsList Recognize()
    {
        GestureActionsList currentAction = new GestureActionsList();
        float currentMin = Mathf.Infinity;

        foreach(var action in _actions)
        {
            float sumDistance = 0;
            bool isDiscarted = false;

            for(int i = 0;i < _bones.Count; i++)
            {
                Vector3 currentData = _skeleton.transform.InverseTransformPoint(_bones[i].Transform.position);
                float distance = Vector3.Distance(currentData, action._gesture.fingersData[i]);
                if(distance > _threshold)
                {
                    isDiscarted = true;
                    break;
                }
                sumDistance += distance;
            }

            if(!isDiscarted && sumDistance < currentMin)
            {
                currentMin = sumDistance;
                currentAction = action;
            }
        }

        return currentAction;
    }
}
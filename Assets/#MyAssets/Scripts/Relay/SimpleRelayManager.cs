using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using System.Threading.Tasks;

// ? IMPORTANTE TENER LOS PAQUETES NECESARIOS, evidentemente... ?

public class SimpleRelayManager : MonoBehaviour
{
    [SerializeField]
    private TMPro.TMP_InputField _codeInput;
    [SerializeField]
    private Button _playButton;
    [SerializeField]
    private Button _createButton;

    private GameObject buttons = null;

    private UnityTransport _unityTransport;
    private const int _maxPlayers = 4;

    private async void Awake()
    {
        buttons = _playButton.transform.parent.gameObject;

        _unityTransport = FindObjectOfType<UnityTransport>();

        buttons.SetActive(false);
        _createButton.onClick.AddListener(CreateRoom);
        _playButton.onClick.AddListener(JoinRoom);

        await Authenticate();

        buttons.SetActive(true);
    }

    private static async Task Authenticate()
    {
        await UnityServices.InitializeAsync();
        await AuthenticationService.Instance.SignInAnonymouslyAsync();
    }

    private async void CreateRoom()
    {
        buttons.SetActive(false);

        try
        {
            Allocation allocation = await RelayService.Instance.CreateAllocationAsync(_maxPlayers);
            _codeInput.text = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);
            _codeInput.enabled = false;

            _unityTransport.SetHostRelayData(allocation.RelayServer.IpV4,
                                      (ushort)allocation.RelayServer.Port,
                                              allocation.AllocationIdBytes,
                                              allocation.Key,
                                              allocation.ConnectionData);

            NetworkManager.Singleton.StartHost();
        }
        catch (RelayServiceException ex)
        {
            Debug.LogError("No se pudo crear la sala: " + ex.Message);

            buttons.SetActive(true);
        }
    }

    private async void JoinRoom()
    {
        buttons.SetActive(false);

        string code = _codeInput.text;

        try
        {
            JoinAllocation joinAllocation = await RelayService.Instance.JoinAllocationAsync(code);

            _unityTransport.SetClientRelayData( joinAllocation.RelayServer.IpV4,
                                        (ushort)joinAllocation.RelayServer.Port,
                                                joinAllocation.AllocationIdBytes,
                                                joinAllocation.Key,
                                                joinAllocation.ConnectionData,
                                                joinAllocation.HostConnectionData);

            NetworkManager.Singleton.StartClient();
        }
        catch(RelayServiceException ex)
        {
            Debug.LogError("Hubo un error al buscar la sala \"" + code + "\". Se encontr� el error:\n" + ex.Message);

            buttons.SetActive(true);
        }
    }
}

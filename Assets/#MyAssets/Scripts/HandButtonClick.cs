using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandButtonClick : MonoBehaviour
{
    /// <summary>
    /// Objects to be randomly instantiated;
    /// </summary>
    [SerializeField]
    private List<GameObject> _spawnList;
    
    /// <summary>
    /// Positions where the objects will spawn randomly.
    /// </summary>
    [SerializeField]
    private List<Transform> _positionsList;

    private bool isInstanciable = true;

    private void Awake()
    {
        if(_spawnList != null && _spawnList.Count > 0 && _positionsList != null && _positionsList.Count > 0)
            isInstanciable = true;
    }

    public void InstanceARandomObject()
    {
        if (!isInstanciable) return;

        int randomObject = Random.Range(0, _spawnList.Count);
        int randomPos = Random.Range(0, _positionsList.Count);

        Instantiate(_spawnList[randomObject], _positionsList[randomPos].position, Quaternion.identity);
    }
}

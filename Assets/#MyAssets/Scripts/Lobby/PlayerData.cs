using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public struct PlayerData : INetworkSerializable, IEquatable<PlayerData>
{
    public ulong ID;
    public bool isReady;

    public PlayerData(ulong iD, bool isReady)
    {
        this.ID = iD;
        this.isReady = isReady;
    }

    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref ID);
        serializer.SerializeValue(ref isReady);
    }

    public bool Equals(PlayerData other)
    {
        return other.ID == ID && other.isReady == isReady;
    }
}

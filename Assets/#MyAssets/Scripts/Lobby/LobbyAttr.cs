using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyAttr : MonoBehaviour
{
    #region VARIABLES
    // LOBBY_VARIABLES
    [HideInInspector]
    public string LobbyName;
    [HideInInspector]
    public string LobbyActualPlayers;
    [HideInInspector]
    public string LobbyMaxPlayers;
    [HideInInspector]
    public string LobbyID;

    //GUI_VARIABLES
    [SerializeField]
    private TMPro.TextMeshProUGUI _playerTxt;
    [SerializeField]
    private TMPro.TextMeshProUGUI _lobbyTxt;
    [SerializeField]
    private Button _joinBtn;
    #endregion END_VARIABLES

    private void OnEnable()
    {
        if( LobbyName != null && LobbyActualPlayers != null && LobbyMaxPlayers != null)
        {
            _lobbyTxt.text = LobbyName;
            _playerTxt.text = LobbyActualPlayers + " / " + LobbyMaxPlayers;
        }

        _joinBtn.onClick.AddListener(OnClick);
    }
    /// <summary>
    /// On lobby line clicked, or join button
    /// </summary>
    private void OnClick()
    {
        if( LobbyRelayManager.Instance != null)
        {
            LobbyRelayManager.Instance.TryConnectLobby(LobbyID);
        }
    }
    
}

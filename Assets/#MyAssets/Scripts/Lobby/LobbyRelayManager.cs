using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using ParrelSync;
#endif

/// <summary>
/// Class that manage connections and creation of Lobby mades with Relay allocations.
/// </summary>
public class LobbyRelayManager : MonoBehaviour
{
    #region VARIABLES
    // GUI_VARIABLES
    [Header("Lobbies list")]
    [SerializeField]
    private GameObject _list;
    [SerializeField]
    private GameObject _listContent;
    [SerializeField]
    private GameObject _lobbyLinePrefab;

    [Header("Buttons")]
    [SerializeField]
    private Button _createBtn;
    [SerializeField]
    private Button _refreshBtn;
    [SerializeField]
    private Button _disconnectBtn;

    private GameObject buttons;

    // FUNCTIONALITY_VARIABLES
    private string _playerID;
    private UnityTransport _unityTransport;
    private const string JoinCodeKey = "M";
    private const int _maxPLayers = 2;
    private Lobby _connectedLobby;

    // SINGLETON_VARIABLES
    private static LobbyRelayManager instance = null;
    public static LobbyRelayManager Instance { get { return instance; } }
    #endregion END_VARIABLES

    #region WORKFLOW
    private async void Awake()
    {
        // Make the GameObject Singleton
        MakeItSingleton();

        _unityTransport = FindObjectOfType<UnityTransport>();

        buttons = _createBtn.transform.parent.gameObject;

        _disconnectBtn.gameObject.SetActive(false);

        buttons.SetActive(false);

        await Authenticate();

        buttons.SetActive(true);

        _createBtn.onClick.AddListener(CreateLobby);
        _refreshBtn.onClick.AddListener(SearchLobbies);
        _disconnectBtn.onClick.AddListener(DisconnectFromLobby);

        // List lobbies at start.
        SearchLobbies();
    }
    /// <summary>
    /// Authenticate the user as anonymous.
    /// </summary>
    private async Task Authenticate()
    {
        var options = new InitializationOptions();

#if UNITY_EDITOR
        options.SetProfile(ClonesManager.IsClone() ? ClonesManager.GetArgument() : "Primary");
#endif

        await UnityServices.InitializeAsync(options);

        await AuthenticationService.Instance.SignInAnonymouslyAsync();
        _playerID = AuthenticationService.Instance.PlayerId;
    }
    /// <summary>
    /// Create a new public lobby, automatically
    /// </summary>
    private async void CreateLobby()
    {
        buttons.gameObject.SetActive(false);
        _list.gameObject.SetActive(false);
        try
        {
            Allocation all = await RelayService.Instance.CreateAllocationAsync(_maxPLayers);
            string Name = await RelayService.Instance.GetJoinCodeAsync(all.AllocationId);

            var options = new CreateLobbyOptions
            {
                Data = new Dictionary<string, DataObject> { { JoinCodeKey, new DataObject(DataObject.VisibilityOptions.Public, Name) } }
            };

            Lobby lobby = await Lobbies.Instance.CreateLobbyAsync(Name, _maxPLayers, options);

            StartCoroutine( Hearthbeat( lobby.Id, 15) );

            _unityTransport.SetHostRelayData( all.RelayServer.IpV4,
                                      (ushort)all.RelayServer.Port,
                                              all.AllocationIdBytes,
                                              all.Key,
                                              all.ConnectionData );

            _connectedLobby = lobby;

            //Scene change to wait room.
            SceneManager.LoadScene(2);

            NetworkManager.Singleton.StartHost();

            _disconnectBtn.gameObject.SetActive(true);

        }
        catch (LobbyServiceException ex)
        {
            buttons.SetActive(true);
            _list.SetActive(true);
            Debug.LogError(ex.Message);
        }
        catch (RelayServiceException ex)
        {
            buttons.SetActive(true);
            _list.SetActive(true);
            Debug.LogError(ex.Message);
        }
    }
    /// <summary>
    /// Calls server every 15 seconds to prevent the lobby to shutdown.
    /// </summary>
    private IEnumerator Hearthbeat(string lobby, int s)
    {
        while(true)
        {
            yield return new WaitForSecondsRealtime(s);
            Lobbies.Instance.SendHeartbeatPingAsync(lobby);
            //Debug.Log(_playerID);
        }
    }
    /// <summary>
    /// Search existing public lobbies and list them in the scroll view.
    /// </summary>
    private async void SearchLobbies()
    {
        var result = await Lobbies.Instance.QueryLobbiesAsync();

        foreach (Transform son in _listContent.transform.GetComponentInChildren<Transform>())
        {
            Destroy(son.gameObject);
        }

        RectTransform trans = (RectTransform)_listContent.transform;
        Vector2 size = trans.sizeDelta;
        size.y = 0;
        trans.sizeDelta = size;

        Debug.Log("Finded " + result.Results.Count + " lobbies.");
        foreach(var lobby in result.Results)
        {
            if( _lobbyLinePrefab.TryGetComponent(out LobbyAttr attr) )
            {
                attr.LobbyName = lobby.Name;
                attr.LobbyMaxPlayers = lobby.MaxPlayers.ToString();
                attr.LobbyActualPlayers = lobby.Players.Count.ToString();
                attr.LobbyID = lobby.Id;

                RectTransform transf = (RectTransform)_listContent.transform;
                Vector2 sizes = transf.sizeDelta;
                sizes.y += 100;
                transf.sizeDelta = sizes;

                Instantiate(_lobbyLinePrefab, _listContent.transform);
            }
            Debug.Log("Finded lobby: " + lobby.Name + " - ID:" + lobby.Id);
        }
    }
    /// <summary>
    /// Atempt to connect to a lobby throught its ID.
    /// </summary>
    public async void TryConnectLobby(string lobbyID)
    {
        buttons.SetActive(false);
        _list.SetActive(false);

        try
        {
            Lobby lobby = await LobbyService.Instance.JoinLobbyByIdAsync(lobbyID);

            var a = await RelayService.Instance.JoinAllocationAsync(lobby.Data[JoinCodeKey].Value);

            _unityTransport.SetClientRelayData( a.RelayServer.IpV4,
                                       (ushort) a.RelayServer.Port,
                                                a.AllocationIdBytes,
                                                a.Key,
                                                a.ConnectionData,
                                                a.HostConnectionData);

            _connectedLobby = lobby;
            NetworkManager.Singleton.StartClient();

            _disconnectBtn.gameObject.SetActive(true);
        }
        catch(LobbyServiceException ex)
        {
            buttons.SetActive(true);
            _list.SetActive(true);
            Debug.LogError(ex.Message);
        }
    }

    private async void DisconnectFromLobby()
    {
        buttons.SetActive(true);
        _list.SetActive(true);
        _disconnectBtn.gameObject.SetActive(false);
        try
        {
            if(_connectedLobby != null)
            {
                StopAllCoroutines();
                if (_connectedLobby.HostId == _playerID)
                {
                    await LobbyService.Instance.DeleteLobbyAsync(_connectedLobby.Id);
                }
                else
                {
                    await LobbyService.Instance.RemovePlayerAsync(_connectedLobby.Id, _playerID);
                }
                _connectedLobby = null;
            }
        }
        catch(LobbyServiceException ex)
        {
            Debug.LogError("Failed trying to disconnect from the Lobby: " + ex.Message);
        }
    }

    private void OnDestroy()
    {
        if(_connectedLobby != null) DisconnectFromLobby();
        Debug.Log(_playerID);
    }
    #endregion END_WORKFLOW

    #region SINGLETON_FUNCTION
    private void MakeItSingleton()
    {
        if(LobbyRelayManager.instance != null)
        {
            if(instance != this)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    #endregion
}
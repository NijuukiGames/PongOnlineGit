using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerList : NetworkBehaviour
{
    private NetworkList<ulong> _connectedPlayersID = new NetworkList<ulong>();

    private void OnEnable()
    {
        Debug.Log("Client: " + IsClient + " - Host: " + IsHost + " - Server: " + IsServer + " - Owner: " + IsOwner);

        if (NetworkManager.Singleton.IsClient && (!NetworkManager.Singleton.IsServer || !NetworkManager.Singleton.IsHost))
            Destroy(this);

        Initialize();
    }
    private async void Initialize()
    {
        //_connectedPlayersID = new NetworkList<ulong>();

        NetworkManager.Singleton.OnClientConnectedCallback += AddClientConnection;
        NetworkManager.Singleton.OnClientDisconnectCallback += RemoveClientConnection;

        StartCoroutine(ShowPlayers());
    }
    private void OnDisable()
    {
        if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost)
        {
            NetworkManager.Singleton.OnClientConnectedCallback -= AddClientConnection;
            NetworkManager.Singleton.OnClientDisconnectCallback -= RemoveClientConnection;
        }
    }

    private void AddClientConnection(ulong ClientID)
    {
        _connectedPlayersID.Add(ClientID);
    }
    private void RemoveClientConnection(ulong ClientID)
    {
        if(_connectedPlayersID.IndexOf(ClientID) >= 0) _connectedPlayersID.RemoveAt(_connectedPlayersID.IndexOf(ClientID));
    }

    private IEnumerator ShowPlayers()
    {
        if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost)
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(5);
                Debug.LogWarning("NM-Loop; -> " + NetworkManager.Singleton.ConnectedClientsIds.Count);
                Debug.LogWarning("Loop; -> " + _connectedPlayersID.Count);
                foreach (ulong player in _connectedPlayersID)
                {
                    Debug.Log("Player: " + player);
                }
            }
        }
    }
}

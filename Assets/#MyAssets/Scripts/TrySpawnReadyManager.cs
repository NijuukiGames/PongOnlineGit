using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class TrySpawnReadyManager : MonoBehaviour
{
    [SerializeField]
    private GameObject readyManager;
    private void Awake()
    {
        if (!NetworkManager.Singleton.IsServer || !NetworkManager.Singleton.IsHost) return;

        var obj = Instantiate(readyManager);
        obj.GetComponent<NetworkObject>().Spawn();
    }
}

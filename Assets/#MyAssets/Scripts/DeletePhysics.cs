using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]

public class DeletePhysics : MonoBehaviour
{
    private Rigidbody rb;
    private void Awake()
    {
        rb.GetComponent<Rigidbody>();
    }

    public void OnSelect()
    {
        rb.useGravity = false;
        rb.isKinematic = true;
    }

    public void OnUnselect()
    {
        rb.useGravity = true;
        rb.isKinematic = false;
    }
}

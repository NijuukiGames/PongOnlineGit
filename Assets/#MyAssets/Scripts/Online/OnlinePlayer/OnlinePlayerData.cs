using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public struct OnlinePlayerData : INetworkSerializable
{
    private ulong _playerID;
    public ulong PlayerID { get { return _playerID; } }

    private string _playerName;
    public string PlayerName { get { return _playerName; } }

    private bool _isReady;
    public bool IsReady{get { return _isReady; } }  

    public OnlinePlayerData(ulong playerID, string playerName, bool isReady)
    {
        _playerID = playerID;
        _playerName = playerName;
        _isReady = isReady;
    }

    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref _playerID);
        serializer.SerializeValue(ref _playerName);
        serializer.SerializeValue(ref _isReady);
    }
}

using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class NetworkClientManager : MonoBehaviour
{
    #region VARIABLES
    private static NetworkClientManager instance = null;
    public static NetworkClientManager Instance { get { return instance; } }

    [Header("Start buttons")]
    [SerializeField]
    private Color _onStartColor = Color.HSVToRGB(103, 100, 69);
    [SerializeField]
    private Color _onNotStartColor = Color.HSVToRGB(355, 100, 69);
    [SerializeField]
    private Button _startOnlineBtn = null;
    [Header("Ready buttons")]
    [SerializeField]
    private Color _onReadyColor = Color.HSVToRGB(103, 100, 69);
    [SerializeField]
    private Color _onNotReadyColor = Color.HSVToRGB(355, 100, 69);
    [SerializeField]
    private List<Button> _readyButtons = null;

    #endregion END_VARIABLES

    #region INIT
    private void Awake()
    {
        MakeSingleton();

        if ( !NetworkManager.Singleton.IsServer && !NetworkManager.Singleton.IsHost )
        {
            _startOnlineBtn.gameObject.SetActive(false);
            return;
        }
        InitializeStartButton();
    }
    private void InitializeStartButton()
    {
        _startOnlineBtn.interactable = false;
        _startOnlineBtn.onClick.AddListener(StartGame);
        VerifyStart();
    }
    #endregion END_INIT

    #region FUNCTIONALLITY
    /// <summary>
    /// Calls he server ChangePlayerState method
    /// </summary>
    public void OnReadyButtonClick()
    {
        if (NetworkServerManager.Instance == null) return;
        NetworkServerManager.Instance.ChangePlayerReady();
    }
    /// <summary>
    /// Change ready buttons on client GUI
    /// </summary>
    public void OnReadyStateChange(ulong playerId, bool state)
    {
        foreach(Button btn in _readyButtons)
        {
            if ( !btn.TryGetComponent(out ReadyOnlineBtn rob) ) continue;
            if ( !rob.ID.Equals(playerId) ) continue;

            if ( btn.TryGetComponent(out Image sprite) )
                sprite.color = state ? _onReadyColor : _onNotReadyColor;

            if ( btn.transform.GetChild(0).TryGetComponent(out TMPro.TextMeshProUGUI tmpro) )
                tmpro.text = state ? "Ready" : "Not ready";
        }
    }

    public void VerifyStart()
    {
        if(NetworkServerManager.Instance == null) return;
        if (_startOnlineBtn.TryGetComponent(out Image sprite)) sprite.color = NetworkServerManager.Instance.AreAllPlayersReady ? _onStartColor : _onNotStartColor;
        _startOnlineBtn.interactable = NetworkServerManager.Instance.AreAllPlayersReady;
    }
    private void StartGame()
    {

    }
    #endregion END_FUNCTIONALLITY

    private void MakeSingleton()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}

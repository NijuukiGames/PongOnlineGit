using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class ReadyManager : NetworkBehaviour
{
    #region VARIABLES
    /// <summary>
    /// Singleton local variable
    /// </summary>
    private static ReadyManager instance = null;
    /// <summary>
    /// Singleton global variable, readonly.
    /// </summary>
    public static ReadyManager Instance { get { return instance; } }

    /// <summary>
    /// Synchronized list of Players data, this contains player ID and a bool to tell if the player is ready or not.
    /// </summary>
    private NetworkList<PlayerData> _clientsID;

    [Header("Ready buttons variables")]
    [SerializeField]
    private Color _onReadyColor = Color.HSVToRGB(103, 100, 69);
    [SerializeField]
    private Color _onNotReadyColor = Color.HSVToRGB(355, 100, 69);
    [SerializeField]
    public List<Button> _readyButtons = new List<Button>();

    /// <summary>
    /// Tell if all clients are ready or not and if there are more than 1 player.
    /// </summary>
    [HideInInspector]
    public bool AreAllReady {
        get {
            if (_clientsID.Count < 2) return false;
                foreach(var client in _clientsID)
                {
                    if (!client.isReady) return false;
                }
                return true;
            } }
    #endregion END_VARIABLES

    #region STARTUP_AND_SHUTDOWN

    /// <summary>
    /// Initialize common values and variables.
    /// </summary>
    private void Awake()
    {
        MakeSingleton();

        _clientsID = new NetworkList<PlayerData>();
        _clientsID.Initialize(this);

        Button btn0 = GameObject.Find("Button0").GetComponent<Button>();
        Button btn1 = GameObject.Find("Button1").GetComponent<Button>();
        _readyButtons[0] = btn0;
        _readyButtons[1] = btn1;
    }
    /// <summary>
    /// Initialize network values and synchronize events.
    /// </summary>
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        if (!IsServer && !IsHost)
        {
            _clientsID.OnListChanged += OnServerListChanged;
            return;
        }

        NetworkManager.Singleton.OnClientConnectedCallback += AddClientConnection;
        NetworkManager.Singleton.OnClientDisconnectCallback += RemoveClientConnection;

        foreach (var client in NetworkManager.Singleton.ConnectedClientsIds)
        {
            AddClientConnection(client);
        }
    }
    /// <summary>
    /// Delete events and relations.
    /// </summary>
    private void OnDisable()
    {
        if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost)
        {
            NetworkManager.Singleton.OnClientConnectedCallback -= AddClientConnection;
            NetworkManager.Singleton.OnClientDisconnectCallback -= RemoveClientConnection;
        }
        else
        {
            _clientsID.OnListChanged -= OnServerListChanged;
        }
    }
    /// <summary>
    /// Delete events, relations and quit a player if necessary
    /// </summary>
    private void OnApplicationQuit()
    {
        if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost)
        {
            NetworkManager.Singleton.OnClientConnectedCallback -= AddClientConnection;
            NetworkManager.Singleton.OnClientDisconnectCallback -= RemoveClientConnection;
        }
        else
        {
            _clientsID.OnListChanged -= OnServerListChanged;

            NetworkManager.Singleton.DisconnectClient(NetworkManager.Singleton.LocalClientId);
        }
    }
    #endregion END_STARTUP_AND_SHUTDOWN

    #region SYNCHRONIZE_AND_FUNCTIONS_METHODS
    private void OnServerListChanged(NetworkListEvent<PlayerData> Client)
    {
        if (_readyButtons.Count >= (int)Client.Value.ID)
        {
            if (_readyButtons[(int)Client.Value.ID].gameObject.TryGetComponent(out Image sprite))
            {
                sprite.color = _clientsID[(int)Client.Value.ID].isReady ? _onReadyColor : _onNotReadyColor;
                if (sprite.transform.GetChild(0).TryGetComponent(out TMPro.TextMeshProUGUI text))
                    text.text = _clientsID[(int)Client.Value.ID].isReady ? "Ready" : "Not ready";
            }
        }
    }
    public void OnPlayerReadyChange(ulong clientID)
    {
        OnPlayerReadyChangeServerRpc(clientID);
    }
    [ServerRpc(RequireOwnership = false)]
    private void OnPlayerReadyChangeServerRpc(ulong clientID = 0, ServerRpcParams serverRpcParams = default)
    {
        _clientsID[(int)clientID] =
            new PlayerData(_clientsID[(int)clientID].ID,
                           !_clientsID[(int)clientID].isReady);

        OnPlayerReadyChangeClientRpc(clientID);
    }
    [ClientRpc]
    private void OnPlayerReadyChangeClientRpc(ulong ClientID)
    {
        if (_readyButtons.Count >= (int)ClientID)
        {
            if ( _readyButtons[(int)ClientID].gameObject.TryGetComponent(out Image sprite) )
            {
                sprite.color = _clientsID[(int)ClientID].isReady ? _onReadyColor : _onNotReadyColor;
                if (sprite.transform.GetChild(0).TryGetComponent(out TMPro.TextMeshProUGUI text))
                    text.text = _clientsID[(int)ClientID].isReady ? "Ready" : "Not ready";
            }
        }
    }

    private void AddClientConnection(ulong ClientID)
    {
        if (!(NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost)) return;

        _clientsID.Add( new PlayerData(ClientID, false) );

        AddClientConnectionClientRpc(ClientID);
    }
    [ClientRpc]
    private void AddClientConnectionClientRpc(ulong ClientID)
    {
        if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost) return;
        
        _clientsID.Add(new PlayerData(ClientID, false));
    }
    private void RemoveClientConnection(ulong ClientID)
    {
        if (!NetworkManager.Singleton.IsServer && !NetworkManager.Singleton.IsHost) return;
        if (_clientsID[(int)ClientID].isReady)
        {
            OnPlayerReadyChange(ClientID);
        }
        for (int i = 0 ; i < _clientsID.Count; i++)
        {
            if (_clientsID[i].Equals(ClientID))
            {
                if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost) 
                    _clientsID.RemoveAt(i);
                return;
            }
        }
        RemoveClientConnectionClientRpc(ClientID);
    }
    [ClientRpc]
    private void RemoveClientConnectionClientRpc(ulong ClientID)
    {
        if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost) return;
        for (int i = 0; i < _clientsID.Count; i++)
        {
            if (_clientsID[i].Equals(ClientID))
            {
                if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost)
                    _clientsID.RemoveAt(i);
                return;
            }
        }
    }
    #endregion END_SYNCHRONIZE_AND_FUNCTIONS_METHODS

    #region TEST_METHODS
    /// <summary>
    /// Show players in the NetworkList to know how many players there are in the list.
    /// </summary>
    private IEnumerator ShowPlayers()
    {
        if (NetworkManager.Singleton.IsServer || NetworkManager.Singleton.IsHost)
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(5);
                Debug.LogWarning("NetworkManager-Loop; -> " + NetworkManager.Singleton.ConnectedClientsIds.Count);
                Debug.LogWarning("HostLoop; -> " + _clientsID.Count);
                foreach (var player in _clientsID)
                {
                    Debug.Log("Player in ClientsID: " + player.ID + " - " + player.isReady);
                }
            }
        }
        else
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(5);
                Debug.LogWarning("ClientLoop; -> " + _clientsID.Count);
                foreach (var player in _clientsID)
                {
                    Debug.Log("Player in ClientsID: " + player.ID + " - " + player.isReady);
                }
            }
        }
    }
    private void Update()
    {
        if (!Input.GetKeyDown(KeyCode.Space)) return;
        Debug.Log("Owner: " + IsOwner + ", Client: " + IsClient + ", Host: " + IsHost + ", Server: " + IsServer);
    }
    #endregion END_TEST_METHODS

    #region START_GAME_METHODS
    /// <summary>
    /// Calls the start of the game when all players are ready.
    /// </summary>
    public void StartGame()
    {
        if (!IsHost && !IsServer) return;
        if (!AreAllReady) return;
        StartGameServerRpc();
    }
    /// <summary>
    /// Redirect to ClientsRpc method to start the game.
    /// </summary>
    [ServerRpc]
    private void StartGameServerRpc(ServerRpcParams serverParams = default)
    {
        StartGameClientRpc();
    }
    /// <summary>
    /// Starts the game scene in all the clients.
    /// </summary>
    [ClientRpc]
    private void StartGameClientRpc(ClientRpcParams clientParams = default)
    {
        // Change to game scene.
    }
    #endregion END_START_GAME_METHODS

    /// <summary>
    /// Makes the class Singleton to make sure there are only one in the scene.
    /// </summary>
    private void MakeSingleton()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //Debug.Log("Self destroy: " + name);
            Destroy(this);
        }
    }
}

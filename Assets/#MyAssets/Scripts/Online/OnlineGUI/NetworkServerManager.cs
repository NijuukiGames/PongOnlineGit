using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class NetworkServerManager : NetworkBehaviour
{
    #region VARIABLES

    private static NetworkServerManager instance = null;
    public static NetworkServerManager Instance { get { return instance; } }

    /// <summary>
    /// Connected players list
    /// </summary>
    private NetworkList<PlayerData> _playersReady;
    public NetworkList<PlayerData> PlayersReady { get { return _playersReady; } }

    public bool AreAllPlayersReady { get
        {
            if (_playersReady.Count < 2) return false;
            foreach(var player in _playersReady)
            {
                if (!player.isReady) return false;
            }
            return true;
        } }

    #endregion END_VARIABLES

    #region INIT
    /// <summary>
    /// Variables and dependencys initialization
    /// </summary>
    private void Awake()
    {
        MakeSingleton();

        _playersReady = new NetworkList<PlayerData>();
        _playersReady.Initialize(this);
    }
    /// <summary>
    /// Network events and related things initializations
    /// </summary>
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        if (!IsServer)
        {
            _playersReady.Clear();
            _playersReady.OnListChanged += OnServerListChanged;
            return;
        }

        if (!NetworkManager.Singleton.IsServer && !NetworkManager.Singleton.IsHost) return;
        NetworkManager.Singleton.OnClientConnectedCallback += AddClientConnection;
        NetworkManager.Singleton.OnClientDisconnectCallback += RemoveClientConnection;

        foreach (var client in NetworkManager.Singleton.ConnectedClientsIds)
        {
            AddClientConnection(client);
        }
    }
    /// <summary>
    /// Unsubscribes from events
    /// </summary>
    private void OnDisable()
    {
        if(!IsServer)
        {
            _playersReady.OnListChanged -= OnServerListChanged;
            return;
        }
        NetworkManager.Singleton.OnClientConnectedCallback -= AddClientConnection;
        NetworkManager.Singleton.OnClientDisconnectCallback -= RemoveClientConnection;
    }
    private void OnApplicationQuit()
    {
        NetworkManager.Singleton.DisconnectClient(NetworkManager.Singleton.LocalClientId);
    }
    #endregion END_INIT

    #region EVENTS
    /// <summary>
    /// Add clients data to the clients list when client connects
    /// </summary>
    private void AddClientConnection(ulong client)
    {
        if (!IsServer) return;
        foreach (var player in _playersReady)
        {
            if (player.ID == client)
            {
                return;
            }
        }

        _playersReady.Add(new PlayerData(client, false));
    }

    /// <summary>
    /// Remove clients data from the list when client disconnects
    /// </summary>
    private void RemoveClientConnection(ulong client)
    {
        if (!IsServer) return;
        for (int i = 0; i < _playersReady.Count; i++)
        {
            if (_playersReady[i].ID.Equals(client))
            {
                if (_playersReady[i].isReady) ChangePlayerReady((long)_playersReady[i].ID);
                _playersReady.RemoveAt(i);
            }
        }
    }

    /// <summary>
    /// Method that execute when there is a change in the server client list data and synchronize players state
    /// </summary>
    private void OnServerListChanged(NetworkListEvent<PlayerData> Client)
    {
        NetworkClientManager.Instance.OnReadyStateChange(Client.Value.ID, Client.Value.isReady);
    }
    #endregion END_EVENTS

    #region FUNCTIONALLITY_AND_METHODS
    /// <summary>
    /// Verify if the player id is a connected player or not
    /// </summary>
    private bool IsPlayerConnected(ulong playerId)
    {
        foreach(var player in _playersReady)
        {
            if (player.ID.Equals(playerId)) return true;
        }
        return false;
    }
    /// <summary>
    /// Calls player change player ready state on server.
    /// </summary>
    public void ChangePlayerReady(long clientId = -1)
    {
        if (clientId < 0) ChangePlayerReadyServerRpc();
        else ChangePlayerReadyServerRpc(clientId);
    }
    /// <summary>
    /// Change player ready state on server and call the button handler
    /// </summary>
    /// <param name="serverRpcParams"></param>
    [ServerRpc(RequireOwnership = false)]
    private void ChangePlayerReadyServerRpc(long playerCId = -1, ServerRpcParams serverRpcParams = default)
    {
        ulong playerId;
        if (playerCId < 0) playerId = serverRpcParams.Receive.SenderClientId;
        else playerId = (ulong)playerCId;

        if ( !IsPlayerConnected(playerId) ) return;

        for (int i = 0; i < _playersReady.Count; i++)
        {
            if (_playersReady[i].ID.Equals(playerId))
            {
                _playersReady[i] = new PlayerData(_playersReady[i].ID, !_playersReady[i].isReady);
                NetworkClientManager.Instance.OnReadyStateChange(_playersReady[i].ID, _playersReady[i].isReady);
                VerifyStart();
                return;
            }
        }
    }
    private void VerifyStart()
    {
        if (!IsServer) return;

        NetworkClientManager.Instance.VerifyStart();
    }

    public void StartGame()
    {
        // STARTS THE GAME
    }

    // UNUSED METHODS

    //public void InstantiateGameObjectWithAuthority(NetworkObject newObject, ulong clientAuthorityId = 0)
    //{
    //    InstantiateGameObjectWithAuthorityServerRpc(newObject, clientAuthorityId);
    //}
    //[ServerRpc(RequireOwnership = false)]
    //private void InstantiateGameObjectWithAuthorityServerRpc(NetworkObject newObject, ulong clientAuthorityId = 0,
    //                                                         ServerRpcParams serverRpcParams = default)
    //{
    //    if (!newObject.TryGetComponent(out NetworkBehaviour nb)) return;
    //    if (!IsPlayerConnected(clientAuthorityId)) return;

    //    var newObj = Instantiate(newObject);
    //    newObj.GetComponent<NetworkBehaviour>().NetworkObject.SpawnWithOwnership(clientAuthorityId);
    //}
    #endregion END_FUNCTIONALLITY_AND_METHODS

    #region TESTS
    private IEnumerator ShowPlayersInList()
    {
        while(true)
        {
            yield return new WaitForSecondsRealtime(5f);
            for(int i = 0; i < _playersReady.Count; i++)
            {
                Debug.Log("Player " + i + "/" + _playersReady.Count + " with ID-" + _playersReady[i].ID +
                                                                      " is ready: " + _playersReady[i].isReady);
            }
        }
    }
    #endregion END_TEST

    private void MakeSingleton()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("Duplicated object destroy");
            Destroy(this.gameObject);
        }
    }
}

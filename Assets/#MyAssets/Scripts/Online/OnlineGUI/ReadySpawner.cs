using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class ReadySpawner : MonoBehaviour
{
    // UNUSED CLASS

    [SerializeField]
    private GameObject _networkReadyManager = null;
    private NetworkObject _network;
    private void Awake()
    {
        if ( _networkReadyManager == null ) return;
        if ( !_networkReadyManager.TryGetComponent(out NetworkObject net) ) return;
        else _network = net;
        //StartCoroutine(FindManager());
    }

    private IEnumerator FindManager()
    {
        NetworkServerManager nsm = null;
        while( (nsm = GameObject.FindObjectOfType<NetworkServerManager>()) == null)
        {
            yield return new WaitForEndOfFrame();
            Debug.Log("Waiting");
        }
        while( !nsm.IsSpawned )
        {
            yield return new WaitForEndOfFrame();
            Debug.Log("Waiting to spawn");
        }
        //nsm.InstantiateGameObjectWithAuthority(_network, NetworkManager.Singleton.LocalClientId);
    }
}

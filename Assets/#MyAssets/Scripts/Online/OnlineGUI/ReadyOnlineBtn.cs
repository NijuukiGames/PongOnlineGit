using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class ReadyOnlineBtn : NetworkBehaviour
{
    [SerializeField]
    private ulong id;
    public ulong ID { get { return id; } }

    private void OnEnable()
    {
        if(this.TryGetComponent(out Button btn))
        {
            if(NetworkManager.Singleton != null)
            {
                if (!NetworkManager.Singleton.LocalClientId.Equals(id))
                {
                    btn.interactable = false;
                }
                btn.onClick.AddListener(OnClick);
            }
        }
    }

    private void OnClick()
    {
        if (NetworkClientManager.Instance != null)
        {
            NetworkClientManager.Instance.OnReadyButtonClick();
        }
    }
}

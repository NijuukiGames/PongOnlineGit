using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class StartOnlineBtn : NetworkBehaviour
{
    //[SerializeField]
    private ulong ID = 0;

    private void OnEnable()
    {
        if(this.TryGetComponent(out Button btn))
        {
            if(NetworkManager.Singleton != null)
            {
                if (!NetworkManager.Singleton.LocalClientId.Equals(ID))
                {
                    btn.interactable = false;
                    btn.gameObject.SetActive(false);
                }
                btn.onClick.AddListener(OnClick);
            }
        }
    }

    private void OnClick()
    {
        // START ACTION
        if(ReadyManager.Instance != null)
        {
            if (ReadyManager.Instance.AreAllReady) ReadyManager.Instance.StartGame();
        }
    }
}

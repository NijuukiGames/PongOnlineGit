using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGesture : MonoBehaviour
{
    [SerializeField]
    private GesturesDetector _gestureDetector;
    public void OnClick()
    {
        _gestureDetector.SaveGesture();
    }
}

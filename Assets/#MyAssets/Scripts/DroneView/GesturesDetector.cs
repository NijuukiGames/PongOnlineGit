using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public struct Gesture
{
    public string name;
    public List<Vector3> fingerDatas;
    public UnityEvent onRecognized;
}

public class GesturesDetector : MonoBehaviour
{
    public OVRSkeleton skeleton;
    public List<Gesture> gestures;
    private List<OVRBone> skeletonBones;


    private void Start()
    {
        skeletonBones = new List<OVRBone>(skeleton.Bones);
        Debug.Log("Skekeleton " + skeleton.name + " has " + skeletonBones.Count + " bones.");
    }

    private void Update()
    {
        if (!Input.GetKeyDown(KeyCode.G)) return;
        SaveGesture();
    }

    public void SaveGesture()
    {
        Gesture g = new Gesture();
        g.name = "New gesture";
        List<Vector3> gestureData = new List<Vector3>();

        foreach(var bone in skeletonBones)
        {
            gestureData.Add(skeleton.transform.InverseTransformPoint(bone.Transform.position));
        }
        g.fingerDatas = gestureData;
        DebugGesture(g);
        SaveHandler.Save(g);
        gestures.Add(g);
    }

    private void DebugGesture(Gesture gesture)
    {
        Debug.Log("Debugging gesture '" + gesture.name + "' with " + gesture.fingerDatas.Count + " bones:");
        for (int n = 0; n < gesture.fingerDatas.Count; n++)
        {
            Vector3 data = gesture.fingerDatas[n];
            Debug.Log("fingerData " + n + ": (" + data.x + ", " + data.y + ", " + data.z + ");");
        }
    }
}

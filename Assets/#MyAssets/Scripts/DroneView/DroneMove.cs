using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class DroneMove : MonoBehaviour
{
    #region VERIABLES

    [SerializeField]
    private float speed = 4f;
    [SerializeField, Range(1f, 10f)]
    private float sensivility = 4f;

    private float horizontal, vertical, fly;
    private float rotationX, rotationY;

    private Vector3 playerForward = Vector3.zero;
    private Vector3 playerUp = Vector3.zero;
    private Vector3 playerRight = Vector3.zero;

    private Rigidbody _rigidbody;

    #endregion END_VERIABLES

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.useGravity = false;
        _rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        //Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        fly = Input.GetAxis("Fly");
        rotationX = Input.GetAxis("Mouse X");
        rotationY = Input.GetAxis("Mouse Y");
    }
    private void FixedUpdate()
    {
        Vector3 playerInput = new Vector3(horizontal, fly, vertical);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);

        camDirection();

        Vector3 move = playerInput.x * playerRight + playerInput.y * playerUp + playerInput.z * playerForward;
        move = move * speed;

        _rigidbody.velocity = move;

        if ( !Input.GetMouseButton(1) ) return;

        if (rotationX != 0)
        {
            this.transform.Rotate(0, rotationX * sensivility, 0);
        }

        GameObject head = Camera.main.gameObject;
        if (head != null)
        {
            Vector2 rotation = new Vector2(-rotationY, rotationX);
            if (rotationY != 0)
            {
                Vector3 cameraRotation = head.transform.localEulerAngles;
                cameraRotation.x = (cameraRotation.x - rotationY * sensivility + 360) % 360;

                if (cameraRotation.x > 60 && cameraRotation.x < 180)
                {
                    cameraRotation.x = 60;
                }
                else if (cameraRotation.x < 300 && cameraRotation.x > 180)
                {
                    cameraRotation.x = 300;
                }
                head.transform.localEulerAngles = cameraRotation;
                //head.transform.Rotate(-rotationY * sensivility, 0, 0);
            }
            //head.transform.Rotate(rotation);
            //head.transform.rotation = Quaternion.Euler(new Vector3(head.transform.rotation.x - rotationY, head.transform.rotation.y + rotationX, 0));
        }
    }

    private void camDirection()
    {
        playerForward = Camera.main.transform.forward;
        playerRight = Camera.main.transform.right;
        playerUp = Camera.main.transform.up;


        //playerForward.y = 0f;
        //playerRight.y = 0f;
        //playerUp

        playerForward = playerForward.normalized;
        playerRight = playerRight.normalized;
        playerUp = playerUp.normalized;

        //this.transform.LookAt(this.transform.position + playerForward);
    }
}

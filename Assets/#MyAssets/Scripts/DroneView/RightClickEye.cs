using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]

public class RightClickEye : MonoBehaviour
{
    [SerializeField]
    private Canvas canvas;
    private Vector2 _canvasSize;

    private Image sprite;


    private void Awake()
    {
        sprite = GetComponent<Image>();
        sprite.enabled = false;

        if (canvas == null) this.gameObject.SetActive(false);
        else _canvasSize = canvas.GetComponent<CanvasScaler>().referenceResolution / 2;
    }

    private void FixedUpdate()
    {
        if (!Input.GetMouseButton(1) && !sprite.enabled) return;
        else if (!Input.GetMouseButton(1)) sprite.enabled = false;
        else
        {
            if ( !sprite.enabled ) sprite.enabled = true;

            Vector2 newPosition = Vector2.zero;
            newPosition.x = (_canvasSize.x * 2 / canvas.pixelRect.size.x) * Input.mousePosition.x - _canvasSize.x;
            newPosition.y = (_canvasSize.y * 2 / canvas.pixelRect.size.y) * Input.mousePosition.y - _canvasSize.y;
            
            ((RectTransform)this.transform).anchoredPosition = newPosition;
        }
    }
}
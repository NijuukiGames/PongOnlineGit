using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    [SerializeField]
    private List<Color> _colors = new List<Color>();

    private int _colorIndex = 0;

    Material _material;

    private void Awake()
    {
        _colorIndex = PlayerPrefs.GetInt("Color001Index", 0);
        _material = new Material(GetComponent<MeshRenderer>().material);
        GetComponent<MeshRenderer>().material = _material;

        if (_colors.Count > _colorIndex) _material.color = _colors[_colorIndex];
        else _colorIndex = 0;
    }

    private void Update()
    {
        if ( !Input.GetKeyDown(KeyCode.T) ) return;
        if ( _colors.Count <= 0) return;

        if (_colorIndex < (_colors.Count - 1)) _colorIndex++;
        else _colorIndex = 0;
        _material.color = _colors[_colorIndex];
    }

    private void OnDisable()
    {
        PlayerPrefs.SetInt("Color001Index", _colorIndex);
    }
}

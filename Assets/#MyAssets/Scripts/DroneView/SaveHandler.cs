using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class SaveHandler
{
    private static string PATH = Directory.GetCurrentDirectory() + @"/Gestures/";
    private static string FILE = PATH+"gestures.json";
    public static void Save(Gesture g)
    {
        if ( !Directory.Exists(PATH) ) Directory.CreateDirectory(PATH);
        if ( !File.Exists(FILE) ) File.Create(FILE).Close();
        
        File.WriteAllText( FILE, JsonUtility.ToJson(g) );
    }
}
